from django.db import models
from django.conf import settings

# Create your models here.

class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)
    # count =

    def count_items(self):
        return len(self.items.all())

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["created_on"]

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(
        default=False,
        choices=((True, 'Yes'), (False, 'No'))
        )
    list = models.ForeignKey(
        TodoList,
        related_name="items",
        on_delete= models.CASCADE,
    )
