from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    todo_list = TodoList.objects.all()

    context = {
        "todo_list": todo_list
    }

    return render(request, "todos/todo_list.html", context)

def todo_item(request, id):
    todo_item = get_object_or_404(TodoList, id=id)

    context = {
        "todo_item": todo_item,
    }

    return render(request, "todos/todo_list_detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
        return redirect("todo_item",id = list.id)
    else:
        form = TodoListForm()

    context = {
        "form":form,
    }
    return render(request, "todos/create_todo_list.html", context)

def update_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = todo_list)
        if form.is_valid():
            form.save()
            # todo_list = form.save(False)
            # todo_list.name = request.name
            # todo_list.save()
            return redirect("todo_item", id=id)
    else:
        form = TodoListForm(instance = todo_list)

    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todos/update_todo_list.html", context)

def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete_todo_list.html")

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save()

        return redirect("todo_list_detail", id= todo.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form":form,

    }
    return render(request, "todos/create_todo_item.html", context)

def update_todo_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = todo_item)
        if form.is_valid:
            item = form.save(False)
            item.todo_item = todo_item
            item.save()
        return redirect("todo_list_create", id=item.list.id)
    else:
        form = TodoItemForm(instance = todo_item)

    context = {
        "form" : form,
        "todo_item" : todo_item
    }
    return render(request, "todos/update_todo_item.html", context)


# def create_todo_item(request, id):
#     if request.method == "POST":
#         form = TodoItemForm(request.post)
#         if form.is_valid():
#             create_item = form.save(False)
#             create_item.todo_list = todo_list
#             create_item.save()
#         return redirect("todo_item", id=id)
#     else:
#         form = TodoItemForm(instance = todo_list)

#     context = {
#         "form":form,
#         "todo_item":todo_item,
#     }

#     return render(request, "todos/create_todo_item.html", context)
